<?php
    $empleados = "SELECT employee_id, CONCAT(fname, ' ', lname, ' - ',title) as Name FROM employee LEFT JOIN person USING (person_id) WHERE title LIKE 'Front Desk' or title LIKE 'Manager';";
    $result = mysqli_query($conn, $empleados);

    $num_results = mysqli_num_rows($result);
    for ($i=0;$i<$num_results;$i++) {
        $row = mysqli_fetch_array($result);
        $code = $row['employee_id'];
        $name = $row['Name'];
        echo '<option value="' .$code. '">' .$name. '</option>';
    }
?>
