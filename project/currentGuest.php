<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

$query = 'SELECT guest_id, CONCAT(fname," ",lname) as Name, TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age, dob as "Date of Birth",   phone AS "Phone number", emergency_phone AS "Emergency Contact", gender as Gender, email, country_name AS Country, bed_id AS Bed, check_in, check_out FROM guest LEFT JOIN person USING (person_id) LEFT JOIN reservation USING (guest_id) LEFT JOIN country USING (country_code) WHERE current LIKE "Y"';

$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

echo"<table border='1'>";
echo"<tr>
        <th>id</th>
        <th>Name</th>
        <th>Age</th>
        <th>Date of birth</th>
        <th>Phone number</th>
        <th>Emergency phone</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Country</th>
        <th>Bed number</th>
        <th>Check in</th>
        <th>Check out</th>
     </tr>";
while($row = mysqli_fetch_assoc($result)){
echo"<tr>
      <td align='center'>{$row['guest_id']}</td>
      <td align='center'>{$row['Name']}</td>
      <td align='center'>{$row['age']}</td>
      <td align='center'>{$row['Date of Birth']}</td>
      <td align='center'>{$row['Phone number']}</td>
      <td align='center'>{$row['Emergency Contact']}</td>
      <td align='center'>{$row['Gender']}</td>
      <td align='center'>{$row['email']}</td>
      <td align='center'>{$row['Country']}</td>
      <td align='center'>{$row['Bed']}</td>
      <td align='center'>{$row['check_in']}</td>
      <td align='center'>{$row['check_out']}</td>
     </tr>";
}
echo"</table>";
mysqli_free_result($result);
mysqli_close($conn);
?>


<br>
<form action="hostel.php">
<input type="submit" value="Return to main menu.">
</form>
