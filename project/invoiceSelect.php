<?php
$out = "SELECT guest_id, CONCAT(fname,' ',lname) as Name FROM guest LEFT JOIN person USING (person_id) LEFT JOIN reservation USING (guest_id) WHERE current = 'Y'";
$result = mysqli_query($conn, $out);

$num_results = mysqli_num_rows($result);
for ($i=0;$i<$num_results;$i++) {
    $row = mysqli_fetch_array($result);
    $code = $row['guest_id'];
    $name = $row['Name'];

    echo '<option value="' .$code. '">' .$name. '</option>';
}
?>
