<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');
?>

<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hostel Administration</title>

</head>

<title>Hostel Administration</title>
<hr>
<form action="add.php" method="POST">
   <fieldset>
    <legend>Add new guest:</legend>
    Id number:<br>
    <input type="number" name="id" required maxlength="8" size="8"> <br><br>
    First name:<br>
    <input type="text" name="first_name" required> <br>
    Last name:<br>
    <input type="text" name="last_name" required> <br>
    Date of birth:<br>
    <input type=date (yyyy-mm-dd) name="dob" required> <br>
    Address:<br>
    <input type="text" name="address" required> <br>
    Phone:<br>
    <input type="number" name="phone" required> <br>
    Emergency phone:<br>
    <input type="number" name="emergency_phone" required> <br>
    Gender:<br>

    <select name="sex">
      <option value = "M">M</option>
      <option value = "F">F</option>
   </select><br>

    Email:<br>
    <input type="text" name="email" required> <br>
    Country:<br>
      <select name="country" required>
        <?php include 'country.php';?>
      </select><br>
    Bed Number:<br>
      <select name="bed" required>
        <?php include 'bed.php';?>
      </select><br>
    Check in:<br>
    <input type="date" name="check_in" required> <br>
    Check out:<br>
    <input type="date" name="check_out" required> <br>
    Employee:<br>

      <select name="employee">
        <?php include 'employee.php';?>
      </select><br>

    <input type="submit" value="Submit">
    <input type="reset" value="Clear"><br>
    *Please, press submit just once and wait.
</fieldset>
</form>


<fieldset>
<legend>Tools:</legend>

<form action="camas.php" method="POST">
<input type="submit" value="Check for free beds">
</form>

<form action="currentGuest.php" method="POST">
<input type="submit" value="Check for current guests">
</form>

<form action="cleanBed.php" method="POST">
<input type="submit" value="Check/update bed status">
</form>

<form action="checkOut.php" method="POST">
<input type="submit" value="Check out guests">
</form>

<form action="invoice.php" method="POST">
<select name="invoice_code">
    <?php include 'invoiceSelect.php';?>
    <input type="submit" value="Check invoice ">
</select>
</form>

</fieldset>




</html>
